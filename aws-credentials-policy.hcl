# Define the policy named "aws-credentials-policy"
path "secret/aws_credentials" {
  capabilities = ["read"]
}
