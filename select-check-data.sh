#!/usr/bin/env bash
# sudo apt-get install postgresql-client -y
# sudo apt install expect -y
export endpoint=$(aws rds describe-db-instances --query "DBInstances[*].Endpoint.Address" --output text | grep post)
export port_num=$(aws rds describe-db-instances --query 'DBInstances[*].[Endpoint.Address,Endpoint.Port]' --output text | grep post | cut -d$'\t' -f2)
export username=$(cat .env | grep -i 'POSTGRES_USER=' | cut -d"=" -f2)
export password=$(cat .env | grep -i password | grep -i postgres | cut -d"=" -f2)
export database=$(cat .env | grep -i POSTGRES_DB | cut -d"=" -f2)
export PGPASSWORD="$password"

# echo "endpoint......$endpoint"
# echo "port_num......$port_num"
# echo "username......$username" 
# echo "password......$password"
# echo "PGPASSWORD....$PGPASSWORD"
# echo "database......$database" 

echo "$endpoint:$port_num:$username:$password" > temp.txt
mv temp.txt ~/.pgpass
chmod 600  ~/.pgpass
# echo "here is my ~/.pgpass"
# cat ~/.pgpass
# echo " " 
# echo "now executing the psql command" 
set -x
#echo '$password' | psql -h $endpoint -U $username -d $database
set +x 
#
# need a sql command that will give me select listings with data 
echo $port_num
# echo "listing from federated_user"
# psql -h $endpoint -U keycloak  -d "keycloak_db" -c 'select * from federated_user' > foo.txt
# cat foo.txt
echo "listing from user_entity"
psql -h $endpoint -U keycloak  -d "keycloak_db" -c 'select * from user_entity' > foo.txt
cat foo.txt

