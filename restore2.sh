#!/bin/bash

# Check if psql is installed
if ! command -v psql &> /dev/null; then
    echo "psql is not installed. Please install it before proceeding."
    exit 1
fi

# Check if .env file exists
if [ ! -f .env ]; then
    echo ".env file not found."
    exit 1
fi

# Source .env file to get database credentials
source .env

# Set default backup file
backup_file="backup_keycloack.sql"

# Check if backup file exists
if [ ! -f "$backup_file" ]; then
    echo "Backup file not found."
    exit 1
fi

# Restore PostgreSQL database
export PGPASSWORD="$POSTGRES_PASSWORD"
psql -U "$POSTGRES_USER" -h "$DB_URL" -d "$POSTGRES_DB" -f "$backup_file"

if [ $? -eq 0 ]; then
    echo "Database restored successfully."
else
    echo "Database restore failed."
    exit 1
fi

unset PGPASSWORD

echo "Database restore completed."

# Perform Docker Compose up
echo "Executing Docker Compose up..."
docker compose up -d

echo "Docker Compose up completed."

