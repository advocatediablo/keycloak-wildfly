# Perform Terraform destroy
echo "Executing Terraform destroy..."
terraform destroy -auto-approve

# Perform Docker Compose down-v
echo "Executing Docker Compose down-v..."
docker-compose down -v

echo "Terraform destroy, and Docker Compose down-v completed."