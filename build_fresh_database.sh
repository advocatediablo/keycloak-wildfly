#!/bin/bash

# Run terraform init
echo "Initializing Terraform..."
terraform init

# Check if init was successful before proceeding
if [ $? -ne 0 ]; then
    echo "Terraform initialization failed. Exiting."
    exit 1
fi

# Run terraform plan
echo "Planning Terraform changes..."
terraform plan

# Check if plan was successful before proceeding
if [ $? -ne 0 ]; then
    echo "Terraform planning failed. Exiting."
    exit 1
fi

# Run terraform apply without asking for confirmation
echo "Applying Terraform changes..."
terraform apply --auto-approve
