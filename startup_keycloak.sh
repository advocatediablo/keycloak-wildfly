#!/bin/bash

echo "Database restore completed."

# Perform Docker Compose up
echo "Executing Docker Compose up..."
docker compose up -d

echo "Docker Compose up completed."

