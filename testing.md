# Keycloak Wildfly

## testing
- testing off site
1. ensured wildfly image jboss 
2. tested working variable names
3. documenting 

## Testing Steps 
<pre>
1. Bring up NEW live system ./build_fresh_database.sh
2. ./startup_keycloak.sh 
3. Make a change that will test the backup
4. Run ./backup.sh
5. Run ./shutdown_keycloak.sh
6. Run ./build_fresh_database.sh

Run restore1.sh

Step 3 
Go to aws and search rds, then copy the endpoint url 

Step 4 
Update your .env change DB_URL with what you copied from the aws 

Step 5
Run restore2.sh

</pre>
## these are the variables that I am using
<pre>
      - POSTGRES_USER
      - POSTGRES_DB
      - POSTGRES_PASSWORD

      - DB_VENDOR
      - DB_DATABASE
      - DB_ADDR
      - DB_SCHEMA
      - DB_USER
      - DB_PASSWORD
      - KEYCLOAK_USER
      - KEYCLOAK_PASSWORD
      - KC_HOSTNAME
      - PROXY_ADDRESS_FORWARDING
      - KC_HTTPS_CERTIFICATE_FILE
      - KC_HTTPS_CERTIFICATE_KEY_FILE

</pre>



## sql 
- see commands 
<pre>
To login to the postgres docker container 

docker exec -it keycloak-wildfly-postgresql-new-1 /bin/bash

To login to postgres cli when inside the container 

psql -U keycloak -d keycloak_db

To show all tables in the database 


\dt

To select all users on the database 

SELECT * FROM user_entity;
</pre>
